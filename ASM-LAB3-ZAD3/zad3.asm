.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
.DATA
		
		cout		   dd ?
		cin			   dd ?

		source		   db 10 dup(0)
		destination	   db 10 dup(?)
		rozmiarD	   dw $ - destination
		tekst          db "Wprowadz liczbe %i :",0
		rozmiar		   dd $ - tekst
		tekst2		   db "Wprowadzone liczby: ",10
		rozmiar2       dd $ - tekst2
		tekst3		   db "Liczby z tablicy",10
		rozmiar3       dd $ - tekst3
	    entero		   dd 10
		rentero        dd $ - entero
		bufor		   dd 10 DUP(?)
		buforZ		   db 10 DUP(?)
		lznakow		   dw  0
		lznakowZ	   dw  0
		liczba		   dd  ?
		tmp            db  ?
		tmp2		   db  ?
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX

	mov EAX, 1
	mov liczba, EAX


	cld
	mov ECX, 10
	mov EDI,OFFSET source
	Pentela:
		push ECX
		push liczba
		push EDI
		invoke wsprintfA, OFFSET bufor, OFFSET tekst, liczba

		invoke WriteConsoleA, cout, OFFSET bufor, rozmiar, OFFSET lznakow, 0
		invoke ReadConsoleA, cin, OFFSET buforZ, 4, OFFSET lznakowZ, 0
		mov al,buforZ
		pop EDI
		stosb
		pop liczba
		inc liczba
		pop ECX
	dec ECX
	jnz Pentela
	mov ECX, 10
	mov ESI, OFFSET source
	odwrot:

	lodsb
	push EAX


	dec ECX
	jnz odwrot

	mov ECX, 10
	mov EDI, OFFSET destination
	zapis:
	
	pop EAX
	stosb

	dec ECX
	jnz zapis
	mov ECX, 10
	mov ESI, OFFSET destination
	wys:
    push ECX
		lodsb
		mov tmp, al
		invoke WriteConsoleA,cout, OFFSET tmp, 2, OFFSET lznakowZ,0

	pop ECX
	dec ECX
	jnz wys


		invoke ExitProcess, 0


main endp

END